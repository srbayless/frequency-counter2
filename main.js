
document.getElementById("countButton").onclick = function() {  
    let typedText = document.getElementById("textInput").value;
    typedText = typedText.toLowerCase();  
 
    typedText = typedText.replace(/[^a-z'\s]+/g, "");  
    const letterCounts = {};
    for (let i = 0; i < typedText.length; i++) {
        currentLetter = typedText[i];
 
    if (letterCounts[currentLetter] === undefined) {
        letterCounts[currentLetter] = 1;  
    } else {  
        letterCounts[currentLetter]++;  
    }
}
    for (let letter in letterCounts) {  
        const span = document.createElement("span");  
        const textContent = document.createTextNode('"' + letter + "\": " + letterCounts[letter] + ", ");  
        span.appendChild(textContent);  
        document.getElementById("lettersDiv").appendChild(span);  
}

const words = typedText.split(/\s/);


const wordsCount = {};
    for (let c = 0; c < words.length; c++) {
        currentword = words[c];
 
    if (wordsCount[currentword] === undefined) {
        wordsCount[currentword] = 1;  
    } else {  
        wordsCount[currentword]++;  
    }
}
    for (let word in wordsCount) {  
        const span = document.createElement("span");  
        const textContent = document.createTextNode('"' + word + "\": " + wordsCount[word] + ", ");  
        span.appendChild(textContent);  
        document.getElementById("wordsDiv").appendChild(span);  
}
}